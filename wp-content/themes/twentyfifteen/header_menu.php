<div class="header_div container">
  <div class="logo_div">
    <img class="logo_img menu_btns" bID="home_btn" src="assets/img/tiara_logo.png">
  </div>
  <div class="empty_div">
  </div>
  <div class="dl_div">
    <span class="dl_btn_span">Download Brochure</span>
  </div>
</div>
<div class="menu_bar_hidden">
  <div class="menu_bar">
  <div class="menu_internal">
      <span class="menu_span menu_btns" bID="over_btn">Project Overview</span>
      <span class="menu_span menu_btns" bID="loc_btn">The Location</span>
      <span class="menu_span menu_btns" bID="models_btn">The Models</span>
      <span class="menu_span menu_btns" bID="constr_btn">Construction Updates</span>
      <span class="menu_span menu_btns" bID="dev_btn">The Developers</span>
      <span class="menu_span menu_btns" bID="contact_btn">Contact Us</span>
    </div>
  </div>
</div>
<div class="fake_head">
</div>
<nav class="navbar navbar-default navbar-fixed-top mobile_nav">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand mobile_links menu_btns" bID="home_btn" href="#home" >
              <img src="assets/img/tiara_logo.png">
            </a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="#features" class="mobile_links menu_btns" bID="over_btn">Project Overview</a></li>
              <li><a href="#models" class="mobile_links menu_btns" bID="loc_btn">The Location</a></li>
              <li><a href="#master_plan" class="mobile_links menu_btns menu_btns_sub_1" bID="models_btn">The Models</a></li>
              <li><a href="#direction_map" class="mobile_links menu_btns menu_btns_sub_2" bID="constr_btn">Construction Updates</a></li>
              <li><a href="#the_developers" class="mobile_links menu_btns" bID="dev_btn">The Developers</a></li>
              <li><a href="#contacts" class="mobile_links menu_btns" bID="contact_btn">Contact Us</a></li>
              <li><a href="#contacts" class="bro_btn dl_btn_span dl_btn_span_menu">Download Brochure</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
