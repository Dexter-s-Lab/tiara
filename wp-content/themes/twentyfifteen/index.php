<?php

/*
 Template Name: Landing Page
 */

 ?>
<?php get_header(); ?>

<?php
global $post;

$p_id = get_the_ID();
$brochure = get_field('brochure',$p_id);

?>

<link href="style_home.css" rel="stylesheet" media="screen">
<link href="assets/css/animate.css" rel="stylesheet" media="screen">


<title>Tiara Residence</title>
<div class="overlay" id="loading">
     <div class="overlay_inner">
      <div class="spinner">
        <div class="ball ball-1"></div>
        <div class="ball ball-2">
          <img class="ball_img" src="assets/img/ball.png">
        </div>
        <div class="ball ball-3">
          <img class="ball_img" src="assets/img/ball.png">
        </div>
        <div class="ball ball-4">
          <img class="ball_img" src="assets/img/ball.png">
        </div>
      </div>
     </div>
  </div>
<div class="main_container" id="top_menu">
  
  <?php include 'header_menu.php' ?>

  <div class="main_div">
    <div class="div_1 main_divs home_btn" id="home">
      <img src="assets/img/main_bg.jpg">
    </div>
    <div class="main_divs proj_overview over_btn" id="proj_overview">
      <img src="assets/img/main_bg.jpg">
      <div class="proj_overview_box">
        <p>With a sense of mission to make luxury affordable for everyone seeking a good living.</p>
        <p>Tiara Residence&trade; introduces to the market its concept of <span>Luxury you can afford</span> by designing luxurious, modernly fully-finished apartment complexes, strategically located at the West side of Cairo and affordably priced to meet the challenges of today's home-buyers.</p>
        <p>Tiara Residence&trade; offers expedited delivery dates, starting June 2017.</p>
      </div>
    </div>
    <div class="div_3 main_divs location loc_btn" id="location">
      <img src="assets/img/location.jpg">
      <div class="location_box">
        <p>Strategically located at the West side of Cairo 6th of October City, conveniently accessible through several main roads; 26th of July Mehwar, Dahshur Entrance from Cairo Alexandria Desert Road and Rod El Farag Mehwar.</p>
        <p>The location is 5 minutes away from Juhayna square, 7 minutes away from Sheikh Zayed City, 10 minutes away from 26th of July Mehwar and just a few minutes away from Dahshur Road.</p>
        <p>Making the location highly accessible to the Heart of West Cairo and connected to the shopping malls, medical centers, schools, universities, business hubs, hyper markets, sporting clubs and more.</p>
      </div>
    </div>
    <div class="div_3 main_divs models models_btn" id="models">
      <img src="assets/img/models_bg.jpg">
      <div class="model_box">
        <p>Tiara Residence&trade; is externally designed with modern high standard facades and luxurious marble entrances, surrounded by wooden fences and tall trees, with the underground garage for our residents.</p>
        <p>Internally, the floor plans are designed modernly with open kitchens, spacious rooms and large windows, all finished in high standards to fit the diverse needs of modern home-buyers.</p>
      </div>
      <div class="model_1_2_head">
        <span>The</span>
        <span>Flats</span>
      </div>
      <img src="assets/img/model_1_1.jpg">
      <div class="model_1_2_head">
        <span>Flat</span>
        <span>Type A</span>
        <span>137.90 SQM</span>
      </div>
      <div class="model_1_2_div">
        <img src="assets/img/model_1_2.jpg">
      </div>
      <div class="model_1_3_head">
        <span>Flat</span>
        <span>Type B</span>
        <span>133.80 SQM</span>
      </div>
      <div class="model_1_3_div">
        <img src="assets/img/model_1_3.jpg">
      </div>
      <div class="model_2_2_head">
        <span>The</span>
        <span>Garden Apartment</span>
        <span>170.20 SQM</span>
      </div>
      <img src="assets/img/model_2_1.jpg">
      <div class="model_2_2_div">
        <img src="assets/img/model_2_2.jpg">
      </div>
      <div class="model_3_2_head">
        <span>The</span>
        <span>Penthouse</span>
        <span>189.50 SQM</span>
      </div>
      <img src="assets/img/model_3_1.jpg">
      <div class="model_3_2_div">
        <img class="model_3_2_div_img" src="assets/img/model_3.png">
      </div>
    </div>
    <div class="div_3 main_divs constr constr_btn" id="constr">
      <div class="slider_div">
        <?php  
          echo do_shortcode('[rev_slider alias="home"]'); 
        ?>
      </div>
    </div>
    <div class="div_3 main_divs devs dev_btn" id="devs">
      <img src="assets/img/devs_bg.jpg">
      <div class="devs_box">
        <p>Tiara Residence&trade; is a project developed by ARD; a company founded by Eng. Ashraf Abouldahab, working in the Real Estate sector since 1984, Abouldahab was one of the main founders of Palm Hills Developments and Haciendas North Coast.</p>
        <p>His Vast Portfolio of developments across Egypt, is the foundation Tiara Residence&trade; is built on.</p>
        <div class="devs_logo_div">
          <img src="assets/img//dev_logo.png">
        </div>
      </div>
    </div>
    <div class="div_3 main_divs contact_div contact_btn" id="contact">
      <div class="contact_box_1 container">
        <div class="contact_left_div">
          <span class="contact_div_span_top">Phone</span>
          <span class="contact_div_span_bot">Address</span>
        </div>
        <div class="contact_right_div">
          <span class="contact_right_div_span_1">002 03 546 22 01</span>
          <span class="contact_right_div_span_1">002 03 546 21 10</span>
          <span class="contact_div_span_bot">13, Gamal Azab street, Alexandria, Egypt.</span>
          <div class="contact_right_inner">
            <span class="contact_right_div_span_4">Abouldahab for Real Estate and Development S.A.E</span>
            <span class="contact_right_div_span_4 contact_right_div_span_3">شركة الارض للإستثمار العقاري ش.م.م</span>
          </div>
        </div>
      </div>
      <div class="send_us_div">
        <span class="contact_box_2_span_1">Send us a message</span>
      </div>
      <form class="form_submit" formID="1">
        <div class="contact_box_2 container">
          <div class="contact_box_2_left">
              <div class="form-group first_form">
                <input inputID="1" type="text" class="form-control contact_form_input form_inputs_1" placeholder="YOUR NAME" required>
              </div>
              <div class="form-group">
                <input inputID="2" type="email" class="form-control contact_form_input form_inputs_1" placeholder="YOUR EMAIL" required>
              </div>
              <div class="form-group">
                <input inputID="3" type="text" class="form-control contact_form_input form_inputs_1" placeholder="YOUR PHONE" required>
              </div>
              <button type="submit" class="btn btn-primary submit_btn submit_1">submit form</button>
          </div>
          <div class="contact_box_2_right">
            <div class="form-group">
              <textarea inputID="4" class="form-control contact_form_text form_inputs_1" rows="3" placeholder="YOUR MESSAGE" required></textarea>
            </div>
            <button type="submit" class="btn btn-primary submit_btn submit_2">submit form</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  
  <?php include 'footer.php' ?>

</div>

<button class="md-trigger md_trigger_sending" data-modal="modal-2"></button>
<button class="md-trigger md_trigger_success" data-modal="modal-3"></button>
<button class="md-trigger md_trigger_form" data-modal="modal-20"></button>

<div class="md-modal md-effect-9" id="modal-20">
  <div class="md-content">
        <div>
            <form class="form-horizontal pdf_submit" formID="2">
            <div class="form-group f_1">
              <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
              <div class="col-sm-12 bro_input_div">
                <input inputID="1" name="name" type="text" class="form-control form_inputs_2 form_name" required>
              </div>
            </div>
            <div class="form-group f_2">
              <label for="inputPassword3" class="col-sm-2 control-label">Email</label>
              <div class="col-sm-12 bro_input_div">
                <input inputID="2" name="email" type="email" class="form-control form_inputs_2 form_email" required>
              </div>
            </div>
            <div class="form-group f_2">
              <label for="inputPassword3" class="col-sm-2 control-label">Phone</label>
              <div class="col-sm-12 bro_input_div">
                <input inputID="3" name="phone" type="text" class="form-control form_inputs_2 form_email" required>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12 pdf_submit_btn_div">
                <div class="send_spin">
                  <span><img src="assets/img/spinner.gif"/>Submitting</span>
                </div>
                <div class="succ_div">
                  <span>Submitted Successfully !</span>
                </div>
                <button type="submit" class="pdf_submit_btn form_btns btn btn-default">Submit</button>
              </div>
              <div class="download_pdf_btn">
                  <a href="<?= $brochure; ?>" download>
                    <input type="button" class="btn btn-default" value="Download"/>
                  </a>
                </div>
            </div>
          </form>
        </div>
      </div>
  <button class="md-close">X</button>
</div>
<div class="md-modal md-effect-2" id="modal-2">
  <div class="md-content">
    <h3><img src="assets/img/spinner.gif"/>Sending</h3>
  </div>
  <button class="md-close"></button>
</div>
<div class="md-modal md-effect-22" id="modal-3">
  <div class="md-content">
    <h3>Message Sent!</h3>
  </div>
  <button class="md-close"></button>
</div>

<div class="md-overlay"></div>
<link rel="stylesheet" type="text/css" href="assets/modal/component.css" />
<script src="assets/modal/modernizr.custom.js"></script>
<script src="assets/modal/classie.js"></script>
<script src="assets/modal/modalEffects.js"></script>

<?php
// gravity_form(1, $display_title=true, $display_description=false, $display_inactive=false, $field_values=null, $ajax=true, 1);
// gravity_form(2, $display_title=true, $display_description=false, $display_inactive=false, $field_values=null, $ajax=true, 1);
?>

<?php get_footer(); ?>

<script>


var newHeight = 0;
var res = 1;

$( document ).ready(function() {

  $('.spinner_logo').removeClass('infinite');
  $('.spinner_logo').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', doSomething);

  function doSomething()
  {
    $('.spinner_logo').addClass('animated rotateOut');
    $('.spinner_logo').removeClass('rotateIn');
    $('.spinner_logo').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', doSomething2);
  }

  function doSomething2()
  {
    $('.spinner_logo').addClass('animated rotateIn');
    $('.spinner_logo').removeClass('rotateOut');
    $('.spinner_logo').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', doSomething);
  }

  var windowWidth = parseInt($(window).width());

  if(windowWidth < 769)
  $('.year_column') .css('top', '83px');
  else
  $('.year_column') .css('top', '90px');  

  var h1 = parseInt($('.news_row').css('height'));
  h1 = h1 - 50;
  h1 = h1 + 'px';
  $('.year_column') .css('height', h1);

  var h2 = parseInt($('.awards_row').css('height'));
  h2 = h2 - 50;
  h2 = h2 + 'px';

  $('.year_column_2') .css('height', h2);

  
  $('.menu_btns').on('click',function (e) {

    e.preventDefault();

    var flag1 = $('.mobile_nav').is(':visible');
    var flag2 = $('.mobile_nav .navbar-collapse').is(':visible');

    if(flag1 && flag2) 
    $('.navbar-toggle').click();


    $('.menu_btns').removeClass('menu_span_active');
    $(this).addClass('menu_span_active');

    $('.main_divs').fadeOut();
    var bId = $(this).attr('bID');

    if(bId == 'dev_btn')
    {
      $('.menu_bar').addClass('menu_bar_2');
    }
    else
    {
      $('.menu_bar').removeClass('menu_bar_2');
    }

    $('.' + bId).fadeIn();
    $('.contact_form_text').css('height', $('.contact_box_2_left').css('height'));

    
    res = 0;
    window.dispatchEvent(new Event('resize'));

    var post = $('#top_menu');
    var position = post.position().top - $(window).scrollTop();
    $('html, body').stop().animate({
        'scrollTop': post.offset().top
    }, 500, 'swing', function () {


      // if(windowWidth > 767)
      // {
      //   newHeight = parseInt($('.header_div').css('height'));
      //   $('.fake_head').css('height', newHeight);
      // }

      // else
      // {
      //   newHeight = parseInt($('.mobile_nav').css('height'));
      //   $('.fake_head').css('height', newHeight);
      // }
        
    });

  });

  $('.dl_btn_span').on('click',function (e) {

    e.preventDefault();

    var flag1 = $('.mobile_nav').is(':visible');
    var flag2 = $('.mobile_nav .navbar-collapse').is(':visible');

    if(flag1 && flag2) 
    $('.navbar-toggle').click();

    $('.md_trigger_form').trigger('click');
    $('.md-close').fadeIn(1800);

  });

  $('.pdf_submit').on('submit',function (e) {

    
    e.preventDefault();

    $('.download_pdf_btn').fadeIn(500);
    $('.f_1').fadeOut(500);
    $('.f_2').fadeOut(500);

   });

  $('.form_submit').on('submit',function (e) {

    
    e.preventDefault();
    return false;
    
    var formID = $(this).attr('formID');
    var inputs = $('.form_inputs_1');

    $.each( inputs, function( key, value ) {
      
      inputID = $(value).attr('inputID');
      var input_old = $(value).val();
      $('#input_' + formID + '_' + inputID).val(input_old);

    });

    var formID = $(this).attr('formID'); 

    var clone = $('#gform_' + formID).clone();
    $('#gform_' + formID).submit();
    $('.footer_div').append(clone);
    
    $('.md_trigger_sending').trigger('click');
    // return false;
    
    // jQuery(document).bind('gform_post_render', function(){
   
    $('.md-close').trigger('click');

    setTimeout(function(){
      $('.md_trigger_success').trigger('click');
    }, 500);

    setTimeout(function(){
      $('.form_inputs_1').val('');
      $('.md-close').trigger('click');
    }, 1500);

    setTimeout(function(){


   }, 

     1500);
    
  // });

   });

});

window.onload = function() {

$('#loading').fadeOut();

$('.menu_bar_hidden').css('height', $('.menu_bar').css('height'));

var windowWidth = $(window).width();

if(windowWidth > 767)
{
  newHeight = parseInt($('.header_div').css('height'));
  $('.fake_head').css('height', newHeight);

  // $('.about_right_col').css('min-height',headerHeight2);
  $('.about_left_col').css('height',parseInt($('.about_right_col').css('height')));
}

else
{
  newHeight = parseInt($('.mobile_nav').css('height'));
  $('.fake_head').css('height', newHeight);
}

  // newHeight = parseInt($('.mobile_nav').css('height'));
  // $('.fake_head').css('height', newHeight);


}

$(window).on('resize', function()
{

  $('.contact_form_text').css('height', $('.contact_box_2_left').css('height'));
  $('.menu_bar_hidden').css('height', $('.menu_bar').css('height'));

  var windowWidth = $(window).width();

  if(res == 1)
  {
    if(windowWidth > 767)
    {
      newHeight = parseInt($('.header_div').css('height'));
      $('.fake_head').css('height', newHeight);
    }

    else
    {
      newHeight = parseInt($('.mobile_nav').css('height'));
      $('.fake_head').css('height', newHeight);
    }
  }

  res = 0;

  var flag1 = $('.mobile_nav').is(':visible');
  var flag2 = $('.mobile_nav .navbar-collapse').is(':visible');

  if(flag1 && flag2) 
  $('.navbar-toggle').click();

  // var head_height = parseInt($('.header_div').css('height'));
  // $('.fake_head').css('height', head_height);

});

$(window).scroll(function() {

    var eTop = $('.main_container').offset().top;
    
    var header_div = parseInt($('.header_div').css('height'));
    // header_div = header_div + 5;
    header_div = -1 * header_div;

    if(parseInt(eTop - $(window).scrollTop()) < header_div)
    {
      $('.menu_bar').addClass('menu_bar_scroll');
      // $('.menu_bar_hidden').show();
    }

    else
    {
      $('.menu_bar').removeClass('menu_bar_scroll');
      // $('.menu_bar_hidden').hide();
    } 

  });

</script>